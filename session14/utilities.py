
# Sometimes we want to stop loop abruptly when some special condition happens and sometimes we have to skip a loop when something special happens

# For these scenarios we have some utility statements called continue and break

# Continue will skip that iteration of loop when executed
# Break will break you out of the loop when that condition happens

#for item in range(0,500,10):
##    print("entering loop")
#    if item%100==0:
#        print("Milestone")
#        continue
#    print(item)
#print("hello")
#


#for num in range(0,100):
#    print(num)
#    if num==5:
#        break
#print("hello")
counter=0
while True:
   counter+=1
   print(counter)
   if counter==10:
       break
