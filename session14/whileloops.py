# While loops are used when we don't know the exact number of iterations we want to run.

# So how does while know when to start or stop.

#While loops runs when the given condition is satisfied. If the condition fails it will exit the loop

# Syntax
# while <condition>:
#   loop content
#   loop content
#   remember loop contents are needs to be indented just like for loops and if conditions

# Let's take a example 
# Most of us used lifts in real life. Inside the lift, you might have noticed what is the maximum weight it can support 

# If the weight is exceeded the lift will not operate until some person left the lift.

#max_weight_supported = 500
#passengers_totalweight=int(input("Enter the total weight of passengers: "))
##!False = True
#while passengers_totalweight<=max_weight_supported:
#    print("Lift will be operated")
#    passengers_totalweight=int(input("Enter the total weight of passengers for next trip: "))
# Now a practical tryout,
# Try to write a python program to get names of players and store it in a list until he types "end"


#sample input

#Rohit
#Paramesh
#Praveen
#end

#list = [Rohit, Paramesh, Praveen]


i=0
flag=True
while flag:
    print("hello")
    i=i+1
    if i==4:
        flag=False
