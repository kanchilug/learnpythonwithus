
# Word analyzer
# Go through the sentence and count the words and print how much each word is repeated.


# Input data in a string. See i Used ''' 
data='''This is the best we can do so we are going to do the procedure and hope for the best'''

#Initializing a empty string
word_counter={}
# Iterating over the words in the string
for word in data.split():
    word_counter[word]=word_counter.get(word, 0)+1
    #Part1 = part2 + part3
    #Part1 is for storing the data in the dict
    #Part2 is for get the value of the key and we have used 0 param this is the default value when the key is stored for first time
    #Part3 is for incrementing everytime a word is read
print(word_counter)
